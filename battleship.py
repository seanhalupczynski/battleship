import random

# Classes

class Ships:
    def __init__(self, name, holes):
        self.name = name
        self.holes = holes

    hits = 0
    
    def hit(self):
        print(f'HIT!')
    
    def sunk(self, shipname):
        print(f'HIT AND SUNK. YOU SUNK MY {shipname}')
    
    location = []

class Gameboard:
    def __init__(self):
        self.gameboard ={
            'row1': ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1'],
            'row2': ['A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2', 'I2'],
            'row3': ['A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3', 'I3'],
            'row4': ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4'],
            'row5': ['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5'],
            'row6': ['A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6', 'I6'],
            'row7': ['A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7', 'I7'],
            'row8': ['A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8', 'I8'],
            'row9': ['A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9', 'I9']
        }


class Player:
    
    def __init__(self, name):
        self.name = name  
        self.game_pieces = {
            'Carrier':{
                'name': 'Carrier',
                'holes': 5,
                'hits': 0,
                'location': [],
                'direction': ''
            },
            'Battleship':{
                'name': 'Battleship',
                'holes': 4,
                'hits': 0,
                'location': [],
                'direction': ''
            },
            'Cruiser':{
                'name': 'Cruiser',
                'holes': 3,
                'hits': 0,
                'location': [],
                'direction': ''
            },
            'Submarine':{
                'name': 'Submarine',
                'holes': 3,
                'hits': 0,
                'location': [],
                'direction': ''
            },
            'Destroyer':{
                'name': 'Destroyer',
                'holes': 2,
                'hits': 0,
                'location': [],
                'direction': ''
            }
        }

        self.guesses = []

        self.my_guess_gameboard = {
            'row1': ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1'],
            'row2': ['A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2', 'I2'],
            'row3': ['A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3', 'I3'],
            'row4': ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4'],
            'row5': ['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5'],
            'row6': ['A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6', 'I6'],
            'row7': ['A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7', 'I7'],
            'row8': ['A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8', 'I8'],
            'row9': ['A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9', 'I9']
        }

        self.my_ships_gameboard = {
            'row1': ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1'],
            'row2': ['A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2', 'I2'],
            'row3': ['A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3', 'I3'],
            'row4': ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4'],
            'row5': ['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5'],
            'row6': ['A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6', 'I6'],
            'row7': ['A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7', 'I7'],
            'row8': ['A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8', 'I8'],
            'row9': ['A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9', 'I9']
        }

        self.gameboard = {
            'row1': ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1'],
            'row2': ['A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2', 'I2'],
            'row3': ['A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3', 'I3'],
            'row4': ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4'],
            'row5': ['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5'],
            'row6': ['A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6', 'I6'],
            'row7': ['A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7', 'I7'],
            'row8': ['A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8', 'I8'],
            'row9': ['A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9', 'I9']
        }

        self.sunken_ships = []
  
    def get_ship_placement(self, ship):
        user_ship_start_location = input(f'Enter the starting point you would like to place your {self.game_pieces[ship]["name"]} [ex: A1]: ').upper()
        user_ship_direction = input(f'Do you want your {self.game_pieces[ship]["name"]} to be horizontal or vertical [h/v]? ').lower()
        self.game_pieces[ship]["direction"] = user_ship_direction
        return user_ship_start_location, user_ship_direction

    def place_ship(self, ship, placement):
        ship_placement =  placement[0]
        for i in range(1, self.game_pieces[ship]["holes"]+1):
            if placement[1] == 'h' and self.valid_location(ship_placement, ship):
                try:
                    self.game_pieces[ship]["location"].append(ship_placement)
                    ship_placement = cols[cols.index(placement[0][0])+i] + placement[0][1]
                except:
                    print(f'End of the board. Re-placing {ship}')
            elif placement[1] == 'v' and self.valid_location(ship_placement, ship):
                try:
                    self.game_pieces[ship]["location"].append(ship_placement)
                    ship_placement = placement[0][0] + str(rows[rows.index(int(placement[0][1])+i)])
                except:
                    print(f'End of the board. Re-placing {ship}')
            else:
                self.game_pieces[ship]["location"] = []
                self.game_pieces[ship]["direction"] = ''
                break
        else:
                print(f'{ship} was placed on the board.')


    def auto_place_ship(self, ship):
        print('='*10, 'Randomly placing ships', '='*10)
        for ship in self.game_pieces:
            print(f'Trying to place {ship}')
            while self.game_pieces[ship]['location'] == []:
                random_col = random.choice(cols)
                random_row = str(random.choice(rows))
                random_direction = random.choice(direction)
                ship_placement = random_col + random_row
                self.game_pieces[ship]['direction'] = random_direction
                for i in range(1, self.game_pieces[ship]['holes']+1):
                    if random_direction == 'h' and self.valid_location(ship_placement, ship):
                        try:
                            self.game_pieces[ship]["location"].append(ship_placement)
                            ship_placement = cols[cols.index(random_col)+i] + random_row
                        except:
                            print(f'End of the board. Re-placing {ship}')
                    elif random_direction == 'v' and self.valid_location(ship_placement, ship):
                        try:
                            self.game_pieces[ship]["location"].append(ship_placement)
                            ship_placement = random_col + str(rows[rows.index(int(random_row)+i)])
                        except:
                            print(f'End of the board. Re-placing {ship}')
                    else:
                        self.game_pieces[ship]["location"] = []
                        self.game_pieces[ship]["direction"] = ''
                        break
                else:
                    print(f'{ship} was placed on the board.')


    def is_ship_on_the_board(self, location):
        if not location[0] in cols or not int(location[1]) in rows and len(list(location)>2):
            return False
        else:
            return True

    def is_space_open(self, location, ship):
        for ships in self.game_pieces:
            if location in self.game_pieces[ships]["location"]:
                print(f'Space occupied. Re-placing {ship}.')
                return False
        return True

    def valid_location(self, location, ship):
        if self.is_ship_on_the_board(location) and self.is_space_open(location, ship):
            return True
        return False

    def make_guess(self, player):
        guess = input('Enter a coordinate [ex: A1]: ').upper()
        while self.has_been_guessed(guess) and self.is_valid_guess(guess):
            guess = input(f'You already guessed {guess} or it is not on the board. Enter another coordinate [ex: A1]: ').upper()
        self.guesses.append(guess)
        self.update_my_guess_gameboard(player, guess)
        player.update_my_ships_gameboard_for_enemy_guess(guess, player)

    def auto_guess(self, player):
        guess = random.choice(cols) + str(random.choice(rows))
        while self.has_been_guessed(guess) and self.is_valid_guess(guess):
            guess = random.choice(cols) + str(random.choice(rows))
        self.guesses.append(guess)
        print(f'{self.name} guessed {guess}.')
        self.is_hit(guess, player)
        player.update_my_ships_gameboard_for_enemy_guess(guess, player)

    def has_been_guessed(self, guess):
        if guess in self.guesses:
            return True
        return False

    def is_valid_guess(self, guess):
        if self.is_ship_on_the_board(guess):
            return True
        return False

    def show_guesses(self):
        print(self.name, 'previous guesses:', ', '.join(self.guesses))

    def is_hit(self, guess, player):
        for ship in player.game_pieces:
            if guess in player.game_pieces[ship]["location"]:
                print('HIT!')
                player.game_pieces[ship]['hits'] += 1
                self.is_sunk(player, ship)
                return True
        print('MISS!')
        return False

    def is_sunk(self, player, ship):
        if player.game_pieces[ship]["hits"] == player.game_pieces[ship]["holes"]:
            print(f'{player.name}\'s {ship} was sunk!')
            self.sunken_ships.append(ship)
            return True
        return False 

    def show_my_guess_gameboard(self):
        for row in self.my_guess_gameboard:
            print(' '.join(self.my_guess_gameboard[row]))

    def update_my_guess_gameboard(self, player, guess):
            if self.is_hit(guess, player):
                for row in self.my_guess_gameboard:
                    if guess in self.my_guess_gameboard[row]:
                        self.my_guess_gameboard[row][self.my_guess_gameboard[row].index(guess)] = 'H '
            else:
                for row in self.my_guess_gameboard:
                    if guess in self.my_guess_gameboard[row]:
                        self.my_guess_gameboard[row][self.my_guess_gameboard[row].index(guess)] = 'M '                

    def show_my_ships_gameboard(self):
        for row in self.my_ships_gameboard:
            print(' '.join(self.my_ships_gameboard[row]))

    def update_my_ships_gameboard_for_ship_placement(self):
        for ship in self.game_pieces:
            for i in range(0, len(self.game_pieces[ship]["location"])):
                for row in self.my_ships_gameboard:
                    if self.game_pieces[ship]["location"][i] in self.my_ships_gameboard[row] and i == 0 and self.game_pieces[ship]["direction"] == 'h':
                        self.my_ships_gameboard[row][self.my_ships_gameboard[row].index(self.game_pieces[ship]["location"][i])] = '<<'
                    elif self.game_pieces[ship]["location"][i] in self.my_ships_gameboard[row] and i == len(self.game_pieces[ship]["location"])-1 and self.game_pieces[ship]["direction"] == 'h':
                        self.my_ships_gameboard[row][self.my_ships_gameboard[row].index(self.game_pieces[ship]["location"][i])] = '>>'
                    elif self.game_pieces[ship]["location"][i] in self.my_ships_gameboard[row] and not i == 0 and not i == len(self.game_pieces[ship]["location"])-1 and self.game_pieces[ship]["direction"] == 'h':
                        self.my_ships_gameboard[row][self.my_ships_gameboard[row].index(self.game_pieces[ship]["location"][i])] = '--'
                    elif self.game_pieces[ship]["location"][i] in self.my_ships_gameboard[row] and i == 0 and self.game_pieces[ship]["direction"] == 'v':
                        self.my_ships_gameboard[row][self.my_ships_gameboard[row].index(self.game_pieces[ship]["location"][i])] = '^^'
                    elif self.game_pieces[ship]["location"][i] in self.my_ships_gameboard[row] and i == len(self.game_pieces[ship]["location"])-1 and self.game_pieces[ship]["direction"] == 'v':
                        self.my_ships_gameboard[row][self.my_ships_gameboard[row].index(self.game_pieces[ship]["location"][i])] = 'vv'
                    elif self.game_pieces[ship]["location"][i] in self.my_ships_gameboard[row] and not i == 0 and not i == len(self.game_pieces[ship]["location"])-1 and self.game_pieces[ship]["direction"] == 'v':
                        self.my_ships_gameboard[row][self.my_ships_gameboard[row].index(self.game_pieces[ship]["location"][i])] = '||'

    def update_my_ships_gameboard_for_enemy_guess(self, guess, player):
        for ship in player.game_pieces:
            if guess in player.game_pieces[ship]['location']:
                for key, values in player.gameboard.items():
                    if guess in values:
                        guess_index = values.index(guess)
                        player.my_ships_gameboard[key][guess_index] = 'XX'

    def player_options(self, player):
        options = input(f'{self.name}, would you like to see your previous guesses [Enter: see guesses], see your ship grid [Enter: see ships], or make a guess [Enter: guess]: ')
        if options == 'see guesses':
            self.show_guesses()
            self.show_my_guess_gameboard()
            return options
        elif options == 'see ships':
            self.show_my_ships_gameboard()
            return options   
        elif options == 'guess':
            self.make_guess(player)
            return options

# Functions
def is_game_over(p1, p2):
    if len(p1.sunken_ships) < 5 and len(p2.sunken_ships) < 5:
        return False
    return True

def get_number_of_players():
    num_of_players = input('To play the computer enter 1. To play with another person enter 2: ')
    return num_of_players

def auto_or_manual_action(name):
    auto_or_manual = input(f'{name}, place you ships automatically [Enter: auto] or place your ships manually [Enter: manual]: ')
    while auto_or_manual not in ['auto', 'manual']:
        auto_or_manual = input(f'{name}, place you ships automatically [Enter: auto] or place your ships manually [Enter: manual]: ')
    return auto_or_manual

def single_player_mode():
    print("single player mode")
    player1 = Player(input('Enter your player name: '))
    computer = Player('Enemy')
    turns = 1

    if auto_or_manual_action(player1.name) == 'manual':
        for ship in player1.game_pieces:
            while not player1.game_pieces[ship]["location"]:
                player1.place_ship(ship, player1.get_ship_placement(ship))
    else:
        for ship in player1.game_pieces:
            while not player1.game_pieces[ship]["location"]:
                player1.auto_place_ship(ship)


    for ship in computer.game_pieces:
        while not computer.game_pieces[ship]["location"]:
            computer.auto_place_ship(ship)

    player1.update_my_ships_gameboard_for_ship_placement()
    computer.update_my_ships_gameboard_for_ship_placement()

    while not is_game_over(player1, computer):
        if turns % 2 == 1:
            player_guess = player1.player_options(computer)
            while not player_guess == 'guess':
                player_guess = player1.player_options(computer)
            turns += 1
        else:
            computer.auto_guess(player1)
            turns += 1

    if len(player1.sunken_ships) == 5:
        print('You won!')
    else:
        print(f'{computer.name} won!')
    
def two_player_mode():
    print("two player mode")
    player1 = Player(input('Enter player1 name: '))
    player2 = Player(input('Enter player2 name: '))
    turns = 1

    if auto_or_manual_action(player1.name) == 'manual':
        for ship in player1.game_pieces:
            while not player1.game_pieces[ship]["location"]:
                player1.place_ship(ship, player1.get_ship_placement(ship))
    else:
        for ship in player1.game_pieces:
            while not player1.game_pieces[ship]["location"]:
                player1.auto_place_ship(ship)

    if auto_or_manual_action(player2.name) == 'manual':
        for ship in player2.game_pieces:
            while not player2.game_pieces[ship]["location"]:
                player2.place_ship(ship, player2.get_ship_placement(ship))
    else:
        for ship in player2.game_pieces:
            while not player2.game_pieces[ship]["location"]:
                player2.auto_place_ship(ship)

    player1.update_my_ships_gameboard_for_ship_placement()
    player2.update_my_ships_gameboard_for_ship_placement()

    while not is_game_over(player1, player2):
        if turns % 2 == 1:
            player_guess = player1.player_options(player2)
            while not player_guess == 'guess':
                player_guess = player1.player_options(player2)
            turns += 1
        else:
            player_guess = player2.player_options(player1)
            while not player_guess == 'guess':
                player_guess = player2.player_options(player1)
            turns += 1

    if len(player1.sunken_ships) == 5:
        print(f'{player1.name} won!')
    else:
        print(f'{player2.name} won!')

def play_battleship():
    print('>'*5, ' '*2, "Welcome to my battleship game. It follows the same rules as normal battleship.")
    print('>'*5, ' '*2, "The object being to sink the other players ships before they sink yours.")
    print('>'*5, ' '*2, "You can play against the computer or another player.")
    print('>'*5, ' '*2, "You will place 5 ships, each takes up a set number of spaces.")
    print('>'*5, ' '*2, "Carrier - 5, Battleshp - 4, Cruiser - 3, Submarine - 3, Destroyer -2")
    print('>'*5, ' '*2, "You will be able to manually place your ships or have them place randomly.", '\n')
    
    number_of_players = ''

    while not number_of_players in ['1', '2']:
        number_of_players = get_number_of_players()

    if number_of_players == '1':
        single_player_mode()
    else:
        two_player_mode()


# Variables
cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']
rows = [1, 2, 3, 4, 5, 6, 7, 8, 9]
direction = ['h', 'v']

player1 = Player('player1')

play_battleship()