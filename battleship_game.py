#! /usr/bin/python3
import re
import random

##### Game setup #####

#Create ships. Can use class or object. Use a class to challenge yourself (also could be the better option).
class Ship:
    def __init__(self, name, holes):
        name = self.name
        holes = self.holes
        hits = 0
        location = []

    def i_am_sunk:
        print(f'You sunk my {self.name}')

#Create a game board for displaying to the user.

#Create a game board to store ship locations. Will need one for each player.
class Gameboard:
    board = {
            rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'],
            cols = [' ', 1, 2, 3, 4, 5, 6, 7, 8, 9]
            }
    spaces = {
            'A': ['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9'],
            'B': ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9'],
            'C': ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9'],
            'D': ['D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9'],
            'E': ['E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9'],
            'F': ['F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9'],
            'G': ['G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9'],
            'H': ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'H9'],
            'I': ['I1', 'I2', 'I3', 'I4', 'I5', 'I6', 'I7', 'I8', 'I9']
            }
    def print_gameboard:
        for col in cols:
            print(col, end=' ')

        for key, value in spaces:
            print(key, end=' ')
                for val in value:
                    print(val, end=' ')

def place_ship(space, direction, piece):
    #space = input('Enter a space as a start point for you ship [ex: A1]: ')
    #direction = input('Do you want this ship to be vertical or horizontal [v/h]? ')
    validate_space(space)
    validate_direction(direction)
    piece.location.append(space)
    for i in range(1, piece.holes):
        if direction[0] == 'h':
            piece.location.append(space[0] + (int(space[1]) + 1))
        else:
            piece.location.append(game_displathougy_board.board.rows[game_display_board.board.rows.index(space[0]) + 1] + space[1])

def get_user_guess():
    user_guess = input('Enter a coordinate [ex: A1]: ')
    while user_guess in prev_user_guesses:
        user_guess = ('You fired their already. Enter a new coordinate: ')
    if user_guess == 'my_ships':
        print(player_gameboard)
    if user_guess == 'my_guesses':
        print(prev_user_guesses)
        print(display_gameboard)
    if not user_guess == 'my_ships' or not user_guess == 'my_guesses':
        prev_user_guesses.append(user_guess)
        validate_space(user_guess)
    for ship in comp_ships:
        if user_guess in comp_ships.location:
            comp_ships.ship.hits += 1
            if comp_ships.ship.hits == comp_ships.ship.holes:
                print(f'HIT AND SUNK! You sunk my {comp_ships[ship][name]}')
            else:
                print('HIT!')
            update_display_gameboard()
        else:
            print('MISS!')
            update_display_gameboard()

def generate_comp_guess():
    comp_guess = random.choice(display_gameboard.rows) + random.choice(display_gamebard.cols[1:])
    prev_comp_guesses.append(comp_guess)
    print(f'Computer guesses {comp_guess}.')
    for ship in user_ships:
        if comp_guess in user_ships.location:
            user_ships.ship.hits += 1
            if user_ships.ship.hits == user_ships.ship.holes:
                print(f'HIT AND SUNK! The enemy sunk your {user_ships[ship][name]}')
            else:
                print(f'Your {ship} was hit!'}
        else:
            print('The enemy missed.'}
#Need the user to input where their ships should go
    #Ships can not go off the board, overlap each other, or be placed diagnally.
    #Ships can only be place horizontally or vertically.
    #If only one player, need to get this info once and randomly generate for the computer. If two players, get info twice.

##### Game play #####

#Get the user's guess.
    #Tell them if it is a hit or miss.
        #If the ship is sunk as a result, tell the user they sunk a ship.
    #Update the ship info that it has been hit.
    #Update the display gameboard and display it.
    #Don't allow user to guess a previous guess. Also might want to display what they have already guessed.

#If two players, repeat the steps above under get user guess.

#Get the computer's guess.
    #Randomly generate a guess for the computer.
        #Cannot repeat a previous guess.
